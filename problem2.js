const findMakeAndModelOfLastCar= function (inventory)
{
    if ( inventory == null || inventory.length == 0){
        console.log(" Data not available ");
      }
      else{
let lastCar = inventory[inventory.length-1];
return `Last Car is a ${lastCar.car_make} ${lastCar.car_model}`;
    }
}


//"Last car is a *car make goes here* *car model goes here*"
module.exports = {

    findMakeAndModelOfLastCar,
    };